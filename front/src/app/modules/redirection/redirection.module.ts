import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RedirectionRoutingModule } from './redirection-routing.module';
import { RedirectionComponent } from './redirection.component';
import { IconsModule } from 'app/core/icons/icons.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    RedirectionComponent
  ],
  imports: [
    CommonModule,
    RedirectionRoutingModule,
    IconsModule,
    MatIconModule,
  ]
})
export class RedirectionModule { }
