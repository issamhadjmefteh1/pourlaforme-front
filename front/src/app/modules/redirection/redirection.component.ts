import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
  selector: 'app-redirection',
  templateUrl: './redirection.component.html',
  styleUrls: ['./redirection.component.scss']
})
export class RedirectionComponent implements OnInit {

  error: Boolean = false;

  constructor(private _authService: AuthService, private _route: Router) { }

  ngOnInit(): void {
  // this._authService.getUserConnecte().subscribe(res => console.log(res));

   if(this._authService.userData.roles.includes("ROLE_GERANT") || this._authService.userData.roles.includes("ROLE_PERSONNEL_ADMINISTRATIF") )
{
      this._route.navigate(['/superAdmin/gerant/ajout']);
} else if (this._authService.userData.roles.includes("ROLE_ADHERENT"))
{
      this._route.navigate(['/superAdmin/planning/list'])
} else if (this._authService.userData.roles.includes("ROLE_PERSONNEL_SPORTIF"))
{
  this._route.navigate(['/superAdmin/planning/list'])
}
  //  else this._route.navigate(['/'])
    // if (this._authService.userData.roles[0] === "ROLE_SUPER_ADMIN") {
    //   this._route.navigate(['/superAdmin/societe/ajout'])
    // } else if (this._authService.userData.roles[0] === "ROLE_ADMIN") {
    //   this._route.navigate(['administrateur/boutique/ajout'])
    // } else if (this._authService.userData.roles[0] === "ROLE_SUPERVISEUR") {
    //   this._route.navigate(['superviseur/boutiques'])
    // } else if (this._authService.userData.roles[0] === "ROLE_MANAGER") {
    //   //this._route.navigate(['manager/boutiques'])
    //   this._route.navigate(['user/dashboards/project'])
    // } else {
    //   if (!this._authService.userData.boutique) {
    //     this.error = true;
    //   } else {
    //     this._route.navigate(['user/dashboards/project'])
    //   }
    // }
  }
}
