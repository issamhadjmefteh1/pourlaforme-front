import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { UserService } from 'app/core/services/user.service';
import { ListAbonnementComponent } from '../list-abonnement/list-abonnement.component';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;
  searchInputControl: FormControl = new FormControl();
  isLoading: boolean = false;
  displayedColumns = ['name', 'lastname','email','role', 'action'];
  users : Array<any> = [];
  dataSource :MatTableDataSource<any>;

  constructor(private userService: UserService,
    private fuseConfirmationDialog:FuseConfirmationService,
    private _matDialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute

    ) { }


    update(id) {
      this.router.navigate(['/superAdmin/gerant/ajout/update', id]);
    }
openListAbonnement(row) {
  let dialog = this._matDialog.open(ListAbonnementComponent, {
    data: { user:row },
    autoFocus: true
  });
dialog.afterClosed().subscribe(res => {this.ngOnInit()});

}


openActivate(row){
  const dialogRef = this.fuseConfirmationDialog.open({
    "title": row.name,
    "message": "vous voulez vraiment desactiver/activer cet utilisateur " + row.name,
    "icon":{
      "show":true,
      "name":"heroicons_outline:exclamation",
      "color":"warn"
    },
    "actions":{
      "cancel":{
        "show": true,
        "label": "Fermer"
      },
      "confirm":{
        "show":true,
        "label":row.actif ? "Désactiver" : "Activer",
        "color":row.actif ? "warn": "primary"
      },
    },
    "dismissible": false
  });
  dialogRef.afterClosed().subscribe(res => {
    if (res === "confirmed"){
      this.userService.activate(row.id).subscribe(res => this.getListeSociete());
    }
  })
}

  getListeSociete(){
    this.userService.getUsers().subscribe(result =>{
      console.log(result)
      this.users = result;
      this.dataSource = new MatTableDataSource<any>(this.users)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

   ngOnInit() {
    this.getListeSociete()

  }
 


applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
}

}
