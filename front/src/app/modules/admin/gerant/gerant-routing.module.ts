import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AjoutPersonnelAdministratifComponent } from './ajout-personnel-administratif/ajout-personnel-administratif.component';
import { ListUsersComponent } from './list-users/list-users.component';

const routes: Routes = [{
  path: 'ajout',
  component:AjoutPersonnelAdministratifComponent
},
{
  path: 'list',
  component: ListUsersComponent
},
{
path: 'ajout/update/:id',
component:AjoutPersonnelAdministratifComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GerantRoutingModule { }
