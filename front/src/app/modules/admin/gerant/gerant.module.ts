import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AjoutPersonnelAdministratifComponent } from './ajout-personnel-administratif/ajout-personnel-administratif.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { IconsModule } from 'app/core/icons/icons.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { GerantRoutingModule } from './gerant-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';
import { ListAbonnementComponent } from './list-abonnement/list-abonnement.component';
import { AjoutAbonnementComponent } from './ajout-abonnement/ajout-abonnement.component';
import { MatDatepicker, MatDatepickerModule, MatDatepickerToggle } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatNativeDateModule } from '@angular/material/core';



@NgModule({
  declarations: [
    AjoutPersonnelAdministratifComponent,
    ListUsersComponent,
    ListAbonnementComponent,
    AjoutAbonnementComponent
  ],
  imports: [
    CommonModule,
    GerantRoutingModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    IconsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    FuseAlertModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatDividerModule,
    MatNativeDateModule    
       
  ]
})
export class GerantModule { }
