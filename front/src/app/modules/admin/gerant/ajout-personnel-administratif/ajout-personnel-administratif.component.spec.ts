import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutPersonnelAdministratifComponent } from './ajout-personnel-administratif.component';

describe('AjoutPersonnelAdministratifComponent', () => {
  let component: AjoutPersonnelAdministratifComponent;
  let fixture: ComponentFixture<AjoutPersonnelAdministratifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutPersonnelAdministratifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutPersonnelAdministratifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
