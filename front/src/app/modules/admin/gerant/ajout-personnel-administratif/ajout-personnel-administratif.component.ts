import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { AuthService } from 'app/core/auth/auth.service';
import { Role } from 'app/core/models/Role';
import { User } from 'app/core/models/user';
import { AbonnementService } from 'app/core/services/abonnement.service';
import { RoleService } from 'app/core/services/role.service';
import { UserService } from 'app/core/services/user.service';
import { AjoutAbonnementComponent } from '../ajout-abonnement/ajout-abonnement.component';

@Component({
  selector: 'app-ajout-personnel-administratif',
  templateUrl: './ajout-personnel-administratif.component.html',
  styleUrls: ['./ajout-personnel-administratif.component.scss']
})
export class AjoutPersonnelAdministratifComponent implements OnInit {

 
  horizontalStepperForm: FormGroup;
  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;
  displayedColumns = ['dateDebut', 'dateFin','duree','libelle', 'tarif'];
  users : Array<any> = [];
  dataSource :MatTableDataSource<any>;
  id: number = null;
  isAdherent: boolean = false;
  user: User = {
    email:'',
    lastname:'',
    password:'',
    roles: [],
    username:''
  };
  roleSelected: Role = {
    id: 0,
    name: ''
  };

  

  roles: Array<any> = [];
  constructor(private _formBuilder: FormBuilder,
    private notifierService: NotifierService,
    private roleService: RoleService,
    private authService: AuthService,
    private abonnementService: AbonnementService,
    private route: ActivatedRoute,
    private userService: UserService,
    private _matDialog: MatDialog,
    private router: Router

    ) { }



  ngOnInit(): void {
    this.user.roles.push(this.roleSelected)
    this.roleService.getRoles().subscribe(res => {
      if (this.authService.userData.roles.includes("ROLE_ADMINISTRATIF"))
        res.forEach(el => {if(el.id != 1 && el.id != 2) this.roles.push(el)})
      else this.roles = res;
      })
    this.horizontalStepperForm = this._formBuilder.group({      
        username: ['',Validators.required],
        email: ['',[Validators.required,Validators.email]],
        lastname: ['', Validators.required],
        role: ['', Validators.required],
        password: ['', Validators.required]
      });

     
     

    this.route.params.subscribe((params: Params) => {
       this.id = params.id;
       if (this.id != null){
       this.userService.getUserById(this.id).subscribe(res => {
         res.roles[0].name == "ROLE_ADHERENT"? this.isAdherent = true : this.isAdherent = false;
         this.user = res;
          this.roleSelected = this.user.roles[0];      
          this.horizontalStepperForm.controls['username'].setValue(this.user.username);
          this.horizontalStepperForm.controls['email'].setValue(this.user.email)
          this.horizontalStepperForm.controls['lastname'].setValue(this.user.lastname)
          this.horizontalStepperForm.controls['role'].setValue(this.user.roles[0].name)
          this.getAbonnementByAderent(this.id);
       })
      }

      });



   

  }


create() {

  console.log(this.horizontalStepperForm.value)

  let user = {
    id:null,
    username: this.horizontalStepperForm.get("username").value,
    email: this.horizontalStepperForm.get("email").value,
    lastname: this.horizontalStepperForm.get("lastname").value,
    roles: [this.horizontalStepperForm.get("role").value.name],
    password: this.horizontalStepperForm.get("password").value
 
  }
  if (this.id){
    user.id = this.id;
    this.userService.updateUser(user).subscribe(res => console.log(res))
  }else { this.authService.signUp(user).subscribe(res => console.log(res))}

 this.router.navigate(['superAdmin/gerant/list'])

}


showNotyf(type:string,text:string){
  this.notifierService.notify(type,text);
}

getAbonnementByAderent(id){
  this.abonnementService.getAbonnementByAderent(id).subscribe(result =>{
    this.users = result;
     this.dataSource = new MatTableDataSource<any>(this.users)
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
   })
 }

 applyFilter(filterValue: string) {
   filterValue = filterValue.trim(); // Remove whitespace
   filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
   this.dataSource.filter = filterValue;
}

ajouterAbonnement() {
  let dialog = this._matDialog.open(AjoutAbonnementComponent, {
    autoFocus: true
  });
  dialog.afterClosed().subscribe(res => {this.ngOnInit()});
}



}
  




