import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AbonnementService } from 'app/core/services/abonnement.service';

@Component({
  selector: 'app-ajout-abonnement',
  templateUrl: './ajout-abonnement.component.html',
  styleUrls: ['./ajout-abonnement.component.scss']
})
export class AjoutAbonnementComponent implements OnInit {

  horizontalStepperForm: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private abonnementService: AbonnementService,
    public matDialogRef: MatDialogRef<AjoutAbonnementComponent>,


  ) { }

  ngOnInit(): void {
    this.horizontalStepperForm = this._formBuilder.group({      
      libelle: ['',Validators.required],
      description: ['',[Validators.required]],
      tarif: ['', Validators.required],
      duree: ['', Validators.required],
      dateDebut: ['', Validators.required],
      dateExpiration: ['', Validators.required]

    });
  }

  create() {
    this.abonnementService.saveAbonnement(this.horizontalStepperForm.value).subscribe(res => console.log(res))

    console.log(this.horizontalStepperForm)

  }

  saveAndClose(): void {
    // Close the dialog
    this.matDialogRef.close();
  }
}
