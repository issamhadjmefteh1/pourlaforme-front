import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AbonnementService } from 'app/core/services/abonnement.service';

@Component({
  selector: 'app-list-abonnement',
  templateUrl: './list-abonnement.component.html',
  styleUrls: ['./list-abonnement.component.scss']
})
export class ListAbonnementComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public matDialogRef: MatDialogRef<ListAbonnementComponent>,
    private abonnementService: AbonnementService,

  ) { }

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;
  displayedColumns = ['dateDebut', 'dateFin','duree','libelle', 'tarif'];
  users : Array<any> = [];
  dataSource :MatTableDataSource<any>;


  ngOnInit(): void {
    this.getListeSociete(this.data.user.id)
  }

  getListeSociete(id){
   this.abonnementService.getAbonnementByAderent(id).subscribe(result =>{
     console.log(result)
     this.users = result;
      this.dataSource = new MatTableDataSource<any>(this.users)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
}


saveAndClose(): void {
  // Close the dialog
  this.matDialogRef.close();
}

}
