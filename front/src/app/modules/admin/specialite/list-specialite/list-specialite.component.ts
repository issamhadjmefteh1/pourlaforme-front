import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { SpecialiteService } from 'app/core/services/specialite.service';

@Component({
  selector: 'app-list-specialite',
  templateUrl: './list-specialite.component.html',
  styleUrls: ['./list-specialite.component.scss']
})
export class ListSpecialiteComponent implements OnInit {

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;
  searchInputControl: FormControl = new FormControl();
  isLoading: boolean = false;
  displayedColumns = ['libelle', 'description', 'action'];
  specialites : Array<any> = [];
  dataSource :MatTableDataSource<any>;

  constructor(
    private fuseConfirmationDialog:FuseConfirmationService,
    private _matDialog: MatDialog,
    private router: Router,
    private specialiteService: SpecialiteService


    ) { }

    getListeSociete(){
        this.specialiteService.getSpecialites().subscribe(result =>{
        console.log(result)
        this.specialites = result;
        this.dataSource = new MatTableDataSource<any>(this.specialites)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    }
  
     ngOnInit() {
      this.getListeSociete()
  
    }
   
  
  
  applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
  }
  
  delete(row) {
    const dialogRef = this.fuseConfirmationDialog.open({
      "title": row.libelle,
      "message": "vous voulez vraiment supprimer cet utilisateur " ,
      "icon":{
        "show":true,
        "name":"heroicons_outline:exclamation",
        "color":"warn"
      },
      "actions":{
        "cancel":{
          "show": true,
          "label": "Fermer"
        },
        "confirm":{
          "show":true,
          "label": "Supprimer",
          "color": "warn"
        },
      },
      "dismissible": false
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res === "confirmed"){
        this.specialiteService.deleteSpecialite(row.idSpec).subscribe(res => this.getListeSociete());
      }
    })
  }

  update(row) {
    this.router.navigate(['/superAdmin/specialite/ajout/update', row.idSpec]);
  }
}
