import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SpecialiteService } from 'app/core/services/specialite.service';

@Component({
  selector: 'app-ajout-specialite',
  templateUrl: './ajout-specialite.component.html',
  styleUrls: ['./ajout-specialite.component.scss']
})
export class AjoutSpecialiteComponent implements OnInit {

  horizontalStepperForm: FormGroup;

  constructor(private _formBuilder: FormBuilder,
    private specialiteService: SpecialiteService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.horizontalStepperForm = this._formBuilder.group({      
      libelle: ['',Validators.required],
      description: ['',[Validators.required]],

    });

    this.route.params.subscribe((params:Params) => {
      if(params.id){
        this.specialiteService.getSpecilites(params.id).subscribe(res =>{
          this.horizontalStepperForm.controls['libelle'].setValue(res.libelle);
          this.horizontalStepperForm.controls['description'].setValue(res.description);


        })      
      }
    });
  }

  create() {
    this.specialiteService.saveSpecialite(this.horizontalStepperForm.value).subscribe(res => console.log(res))

    console.log(this.horizontalStepperForm)
    this.router.navigate(['/superAdmin/specialite/list'])

  }
}
