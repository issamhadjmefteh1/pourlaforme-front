import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AjoutSpecialiteComponent } from './ajout-specialite/ajout-specialite.component';
import { ListSpecialiteComponent } from './list-specialite/list-specialite.component';

const routes: Routes = [
  {
    path: 'ajout',
    component: AjoutSpecialiteComponent
  },
  {
    path: 'list',
    component: ListSpecialiteComponent
  },
  {
    path: 'ajout/update/:id',
    component: AjoutSpecialiteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialiteRoutingModule { }
