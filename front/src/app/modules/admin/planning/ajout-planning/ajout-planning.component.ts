import { SeanceService } from './../../../../core/services/seance.service';
import { SalleDeCoursService } from './../../../../core/services/salle-de-cours.service';
import { PlanningService } from './../../../../core/services/planning.service';
  import { Component, OnInit } from '@angular/core';
  import { FormBuilder, FormGroup, Validators } from '@angular/forms';
  import { MatDialogRef } from '@angular/material/dialog';
  import { AbonnementService } from 'app/core/services/abonnement.service';

@Component({
  selector: 'app-ajout-planning',
  templateUrl: './ajout-planning.component.html',
  styleUrls: ['./ajout-planning.component.scss']
})
export class AjoutPlanningComponent implements OnInit {
  
    horizontalStepperForm: FormGroup;
    listSalleDeCours: Array<any> = [];
    listSeances: Array<any> = [];
    seance;
    constructor(
      private _formBuilder: FormBuilder,
      private planningService: PlanningService,
      private seanceService: SeanceService, 
      private salleDeCoursService: SalleDeCoursService,
  
    ) { }
  
    ngOnInit(): void {
      this.horizontalStepperForm = this._formBuilder.group({  
        salleDeCours:[''],    
        seance: ['', Validators.required],
        heureFin: ['', Validators.required],
        datePlannif: ['', Validators.required],
        heureDebut: ['', Validators.required]
        
      });
    //  this.salleDeCoursService.getListSalleDeCoursBySalleDeSport(1).subscribe(res => this.listSalleDeCours= res)
    }


   /* getListSalleDeCoursBySalleDeSport(id){
      this.salleDeCoursService.getListSalleDeCoursBySalleDeSport(id).subscribe(res => console.log(res))
    }*/

    getListSeancesBySalleDeCours(value){
    this.seanceService.getListSeancesByIdSalleCours(value).subscribe(res => this.listSeances = res);
    } 
  
    create() {
      this.planningService.addPlanning(this.horizontalStepperForm.value).subscribe(res => console.log(res))
  
      console.log(this.horizontalStepperForm)
  
    }
  
    /*saveAndClose(): void {
      // Close the dialog
      this.matDialogRef.close();
    }*/
    annuler(){
      this.horizontalStepperForm.reset();
    }
  }
  
