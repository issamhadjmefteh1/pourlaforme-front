import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutPlanningComponent } from './ajout-planning.component';

describe('AjoutPlanningComponent', () => {
  let component: AjoutPlanningComponent;
  let fixture: ComponentFixture<AjoutPlanningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutPlanningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
