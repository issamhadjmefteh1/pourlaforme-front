import { PlanningService } from './../../../../core/services/planning.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { UserService } from 'app/core/services/user.service';

@Component({
  selector: 'app-list-planning',
  templateUrl: './list-planning.component.html',
  styleUrls: ['./list-planning.component.scss']
})
export class ListPlanningComponent implements OnInit {

  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;
  searchInputControl: FormControl = new FormControl();
  isLoading: boolean = false;


  
  displayedColumns = ['nomSalleCours','libelleSeance','prenomEntraineur', 'datePlannif','heureDebut','heureFin','action'];
  plannings : Array<any> = [];
  dataSource :MatTableDataSource<any>;

  constructor(private planningService: PlanningService,
    private fuseConfirmationDialog:FuseConfirmationService,
    private _matDialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute

    ) { }


    update(id) {
      this.router.navigate(['/superAdmin/gerant/ajout/update', id]);
    }
openListAbonnement(row) {
  let dialog = this._matDialog.open(ListPlanningComponent, {
    data: { user:row },
    autoFocus: true
  });
dialog.afterClosed().subscribe(res => {this.ngOnInit()});

}


/*openActivate(row){
  const dialogRef = this.fuseConfirmationDialog.open({
    "title": row.name,
    "message": "vous voulez vraiment desactiver/activer cet utilisateur " + row.name,
    "icon":{
      "show":true,
      "name":"heroicons_outline:exclamation",
      "color":"warn"
    },
    "actions":{
      "cancel":{
        "show": true,
        "label": "Fermer"
      },
      "confirm":{
        "show":true,
        "label":row.actif ? "Désactiver" : "Activer",
        "color":row.actif ? "warn": "primary"
      },
    },
    "dismissible": false
  });
  dialogRef.afterClosed().subscribe(res => {
    if (res === "confirmed"){
      this.userService.activate(row.id).subscribe(res => this.getListeSociete());
    }
  })
}*/

getListPlanningByIdSalle(){
    let id=1;
    this.planningService.getListPlanningByIdSalle(id).subscribe(result =>{
      console.log(result)
      this.plannings = result;
      this.dataSource = new MatTableDataSource<any>(this.plannings)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

   ngOnInit() {
    this.getListPlanningByIdSalle()

  }
 


applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
}


}
