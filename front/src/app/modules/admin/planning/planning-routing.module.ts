import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AjoutPlanningComponent } from './ajout-planning/ajout-planning.component';
import { ListPlanningComponent } from './list-planning/list-planning.component';

const routes: Routes = [
  {
    path: 'ajout',
    component: AjoutPlanningComponent
  },
  {
    path: 'list',
    component: ListPlanningComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningRoutingModule { }
