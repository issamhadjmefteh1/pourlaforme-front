import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GerantModule } from './gerant/gerant.module';
import { PlanningModule } from './planning/planning.module';
import { SpecialiteModule } from './specialite/specialite.module';
import { EquipementModule } from './equipement/equipement.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GerantModule,
    PlanningModule,
    SpecialiteModule,
    EquipementModule
    
  ]
})
export class AdminModule { }
