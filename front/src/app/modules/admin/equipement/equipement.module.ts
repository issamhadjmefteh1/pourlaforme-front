import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EquipementRoutingModule } from './equipement-routing.module';
import { ListEquipementComponent } from './list-equipement/list-equipement.component';
import { AjoutEquipementComponent } from './ajout-equipement/ajout-equipement.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseAlertModule } from '@fuse/components/alert';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { IconsModule } from 'app/core/icons/icons.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { MatDatepicker, MatDatepickerModule, MatDatepickerToggle } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatNativeDateModule } from '@angular/material/core';




@NgModule({
  declarations: [
    ListEquipementComponent,
    AjoutEquipementComponent
  ],
  imports: [
    CommonModule,
    EquipementRoutingModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    IconsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    FuseAlertModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatDividerModule,
    MatNativeDateModule,
  ]
})
export class EquipementModule { }
