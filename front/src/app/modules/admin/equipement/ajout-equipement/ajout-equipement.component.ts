import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { EquipementService } from 'app/core/services/equipement.service';

@Component({
  selector: 'app-ajout-equipement',
  templateUrl: './ajout-equipement.component.html',
  styleUrls: ['./ajout-equipement.component.scss']
})
export class AjoutEquipementComponent implements OnInit {

  horizontalStepperForm: FormGroup;

  constructor(private _formBuilder: FormBuilder,
    private equipementService: EquipementService,
    private route: ActivatedRoute,

    ) { }

  ngOnInit(): void {
    this.horizontalStepperForm = this._formBuilder.group({      
      libelle: ['',Validators.required],
      description: ['',[Validators.required]],
      dateAchat: ['', Validators.required]
    });

    this.route.params.subscribe((params:Params) => {
      if(params.id){
        this.equipementService.getEquipement(params.id).subscribe(res =>{
          this.horizontalStepperForm.controls['libelle'].setValue(res.libelle);
          this.horizontalStepperForm.controls['description'].setValue(res.description);
          this.horizontalStepperForm.controls['dateAchat'].setValue(res.dateAchat);    
        })      
      }
    });
  }

  create() {
    this.equipementService.saveEquipement(this.horizontalStepperForm.value).subscribe(res => console.log(res))

    console.log(this.horizontalStepperForm)

  }

}


