import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AjoutEquipementComponent } from './ajout-equipement/ajout-equipement.component';
import { ListEquipementComponent } from './list-equipement/list-equipement.component';

const routes: Routes = [
  {
    path: 'ajout',
    component: AjoutEquipementComponent
  },
  {
    path: 'list',
    component: ListEquipementComponent
  },
  {
    path: 'ajout/update/:id',
    component: AjoutEquipementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipementRoutingModule { }
