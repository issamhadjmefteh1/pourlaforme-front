import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignUpComponent implements OnInit
{
    @ViewChild('signUpNgForm') signUpNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    
    signUpForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signUpForm = this._formBuilder.group({
                username      : ['', Validators.required],
                email     : ['', [Validators.required, Validators.email]],
                password  : ['', Validators.required],
                reppassword   : ['', Validators.required],
                
            },{
                validator: this.MustMatch('password','reppassword')
              }
        );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign up
     */
    signUp(): void
    {
        // Do nothing if the form is invalid
        if ( this.signUpForm.invalid )
        {
            return;
        }

        // Disable the form
        this.signUpForm.disable();

        // Hide the alert
        this.showAlert = false;

        let user = {
            login: this.signUpForm.get("username").value,
            email: this.signUpForm.get("email").value,
            password: this.signUpForm.get("password").value,
            langKey: "fr"
        }

        // Sign up
        // this._authService.signUp(user)
        //     .subscribe(
        //         (response) => {

        //             // Navigate to the confirmation required page
                    
        //             //this._router.navigateByUrl('/confirmation-required');
        //             this.signUpForm.enable();
                    
        //             // Set the alert
        //             this.alert = {
        //                 type   : 'success',
        //                 message: 'Votre compte est crée avec succèes'
        //             };

        //             this.showAlert = true;
        //         },
        //         (response) => {
        //             console.log(response.error.errorKey)
        //             switch (response.error.errorKey) {
        //                 case "userexists": {
        //                     console.log(response.error.errorKey)
        //                     this.alert = {
        //                         type   : 'error',
        //                         message: 'Erreur, user exist'
        //                     };
        //                 }break;
        //                 case "emailexists": {
        //                     this.alert = {
        //                         type   : 'error',
        //                         message: 'Erreur, email exist'
        //                     };
        //                 }break;
        //                 default: {
        //                     this.alert = {
        //                         type   : 'error',
        //                         message: 'Erreur, password must be between 4 to 100 characters'
        //                     };
        //                 }
        //             }


        //             // Re-enable the form
        //             this.signUpForm.enable();

        //             // Reset the form
        //           //  this.signUpNgForm.resetForm();

        //             // Set the alert
        //             // this.alert = {
        //             //     type   : 'error',
        //             //     message: 'Erreur, les données sont erronées'
        //             // };

        //             // Show the alert
        //             this.showAlert = true;
        //         }
        //     );
    }


    MustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
    
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
    
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            } else {
                matchingControl.setErrors(null);
            }
        }
    }
}
