import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector     : 'auth-confirmation-required',
    templateUrl  : './confirmation-required.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthConfirmationRequiredComponent implements OnInit
{
    /**
     * Constructor
     */
    constructor(private _activateRoute: ActivatedRoute,
                private _authService: AuthService)
    {
    }

    ngOnInit(): void {
        this._activateRoute.queryParams
    .subscribe((params: Params) => {

        this._authService.activateAccount(params.key).subscribe(res=> console.log(res));

    });
    }
}
