import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExtraOptions, PreloadAllModules, RouterModule } from '@angular/router';
import { MarkdownModule } from 'ngx-markdown';
import { FuseModule } from '@fuse';
import { FuseConfigModule } from '@fuse/services/config';
//import { FuseMockApiModule } from '@fuse/lib/mock-api';
import { CoreModule } from 'app/core/core.module';
import { appConfig } from 'app/core/config/app.config';
//import { mockApiServices } from 'app/mock-api';
import { LayoutModule } from 'app/layout/layout.module';
import { AppComponent } from 'app/app.component';
import { appRoutes } from 'app/app.routing';
import { NotifierModule } from 'angular-notifier';
import { notifierDefaultOptions } from './core/utils/notyf';
import { TimeagoModule } from 'ngx-timeago';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr-CH';
import { AdminModule } from './modules/admin/admin.module';

const routerConfig: ExtraOptions = {
    preloadingStrategy       : PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled'
};

registerLocaleData(localeFr);

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes, routerConfig),

        // Fuse, FuseConfig & FuseMockAPI
        FuseModule,
        FuseConfigModule.forRoot(appConfig),
       // FuseMockApiModule.forRoot(mockApiServices),

        // Core module of your application
        CoreModule,
        AdminModule,

        // Layout module of your application
        LayoutModule,

        // 3rd party modules that require global configuration via forRoot
        MarkdownModule.forRoot({}),

        NotifierModule.withConfig(notifierDefaultOptions),

        TimeagoModule.forRoot()

        
    ],
    bootstrap   : [
        AppComponent
    ],
    providers: [
        { provide: LOCALE_ID, useValue: "fr-ch" }, //replace "de-at" with your locale
        //otherProviders...
      ]
   
})
export class AppModule
{
}
