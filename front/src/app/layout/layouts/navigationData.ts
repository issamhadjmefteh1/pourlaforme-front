import { FuseNavigationItem } from "@fuse/components/navigation";
export const superNavigation: FuseNavigationItem[] = [

    {
        id: 'gestion',
        title: 'Gestion Utilisateurs',
        type: 'collapsable',
        icon: 'heroicons_outline:user',
        exactMatch: true,
        children: [
        {
            id: 'ajout',
            title: 'Ajouter Utilisateurs',
            type: 'basic',
            icon: 'heroicons_outline:plus',
            link: '/superAdmin/gerant/ajout'
        },
        {
            id: 'liste',
            title: 'Liste des Utilisateurs',
            type: 'basic',
            icon: 'heroicons_outline:menu',
            link: '/superAdmin/gerant/list'
        },
]},

{
    id: 'gestion',
    title: 'Gestion Planning',
    type: 'collapsable',
    icon: 'heroicons_outline:user',
    exactMatch: true,
    children: [
    {
        id: 'ajout',
        title: 'Ajouter Planning',
        type: 'basic',
        icon: 'heroicons_outline:plus',
        link: '/superAdmin/planning/ajout'
    },
    {
        id: 'liste',
        title: 'Liste des Planning',
        type: 'basic',
        icon: 'heroicons_outline:menu',
        link: '/superAdmin/planning/list'
    },
]},
{
    id: 'gestion',
    title: 'Gestion Equipement',
    type: 'collapsable',
    icon: 'heroicons_outline:user',
    exactMatch: true,
    children: [
    {
        id: 'ajout',
        title: 'Ajouter Equipement',
        type: 'basic',
        icon: 'heroicons_outline:plus',
        link: '/superAdmin/equipement/ajout'
    },
    {
        id: 'liste',
        title: 'Liste des Equipement',
        type: 'basic',
        icon: 'heroicons_outline:menu',
        link: '/superAdmin/equipement/list'
    },
]},
{
    id: 'gestion',
    title: 'Gestion Spécialité',
    type: 'collapsable',
    icon: 'heroicons_outline:user',
    exactMatch: true,
    children: [
    {
        id: 'ajout',
        title: 'Ajouter Spécialité',
        type: 'basic',
        icon: 'heroicons_outline:plus',
        link: '/superAdmin/specialite/ajout'
    },
    {
        id: 'liste',
        title: 'Liste des Spécialité',
        type: 'basic',
        icon: 'heroicons_outline:menu',
        link: '/superAdmin/specialite/list'
    },
]},

// {
//     id: 'gestion',
//     title: 'Planning',
//     type: 'collapsable',
//     icon: 'heroicons_outline:user',
//     exactMatch: true,
//     children: [
//     {
//         id: 'ajout',
//         title: 'consulter planning',
//         type: 'basic',
//         icon: 'heroicons_outline:home',
//     //    link: '/superAdmin/gerant/ajout'
//     },
//     {
//         id: 'ajout',
//         title: 'Réserver une place',
//         type: 'basic',
//         icon: 'heroicons_outline:home',
//     //    link: '/superAdmin/gerant/ajout'
//     },
  
// ]},
// {
//     id: 'gestion',
//     title: 'Abonnements',
//     type: 'collapsable',
//     icon: 'heroicons_outline:user',
//     exactMatch: true,
//     children: [
//     {
//         id: 'ajout',
//         title: 'consulter Abonnements',
//         type: 'basic',
//         icon: 'heroicons_outline:home',
//     //    link: '/superAdmin/gerant/ajout'
//     },
  
// ]},




    // {
    //     id:'ajoutProp',
    //     title: 'Ajout Property',
    //     type: 'basic',
    //     icon: 'heroicons_outline:plus',
    //     link: '/superAdmin/property/ajout'
    // }
 ]
export const adherentNavigation: FuseNavigationItem[] = [
    {
        id: 'gestion',
        title: 'Planning',
        type: 'collapsable',
        icon: 'heroicons_outline:user',
        //link: '/apps/help-center',
        exactMatch: true,
        children: [
            {
                id: 'ajoutManager',
                title: 'Consulter Planning',
                type: 'basic',
                icon: 'heroicons_outline:user-add',
                link: '/superAdmin/planning/list',
                exactMatch: true
            },
           
            /*{
                id: 'listManager',
                title: 'Liste des Managers',
                type: 'basic',
                icon: 'heroicons_outline:user-add',
                link: '/administrateur/boutique/Manager/liste',
                exactMatch: true
            }*/
        ]
    },
]

export const personnelSportifNavigation: FuseNavigationItem[] = [

    {
        id: 'gestion',
        title: 'Planning',
        type: 'collapsable',
        icon: 'heroicons_outline:user',
        //link: '/apps/help-center',
        exactMatch: true,
        children: [
            {
                id: 'ajoutManager',
                title: 'Consulter Planning',
                type: 'basic',
                icon: 'heroicons_outline:user-add',
                link: '/superAdmin/planning/list',
                exactMatch: true
            },
        ]
    },
    {
        id: 'gestion',
        title: 'Equipement',
        type: 'collapsable',
        icon: 'heroicons_outline:user',
        exactMatch: true,
        children: [
        {
            id: 'liste',
            title: 'Liste des Equipement',
            type: 'basic',
            icon: 'heroicons_outline:menu',
            link: '/superAdmin/equipement/list'
        },
    ]},
]

// export const superviseurdashboard: FuseNavigationItem[] = [

//     {
//         id: 'listeBoutiques',
//         title: 'Liste des Boutiques',
//         type: 'basic',
//         icon: 'heroicons_outline:menu',
//         link: '/superviseur/boutiques'
//     },
// ]

// export function adminDashNav(stat: any) {
//     const adminDashNavigation: FuseNavigationItem[] = [
//         {
//             id: 'acceuil',
//             title: 'Acceuil',
//             type: 'basic',
//             icon: 'heroicons_outline:home',
//             link: '/user/dashboards/project'
//         },
//         {
//             id: 'diffusions',
//             title: 'Notifications',
//             type: 'collapsable',
//             icon: 'heroicons_outline:chart-pie',
//             badge: {
//                 title: (stat.DemandeProduit.Bystatut.Encours ?? 0) + (stat.DemandeProduit.Bystatut.Virement ?? 0) + (stat.DemandeConsommable.Entransit ?? 0)
//                     + (stat.DemandeReassort.Bystatut.Encours ?? 0) + (stat.DemandeReassort.Bystatut.Virement ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             // link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Produits',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:chart-pie',
//                     //link: '/apps/help-center',
//                     //exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Encours',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/produits/encours',
//                             badge: {
//                                 title: stat.DemandeProduit.Bystatut.Encours ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                             //exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/produits/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeProduit.Bystatut.Virement ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Consommable',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:chart-pie',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/consommable/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeConsommable.Entransit ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Réassort',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:user',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Encours',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/reassort/encours',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeReassort.Bystatut.Encours ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/reassort/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeReassort.Bystatut.Virement ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//             ],
//         },
//         {
//             id: 'lien',
//             title: 'Liens',
//             type: 'basic',
//             icon: 'heroicons_outline:link',
//             link: '/user/liens'
//         },
//         {
//             id: 'diffusions',
//             title: 'Les diffusions',
//             type: 'collapsable',
//             icon: 'heroicons_outline:share',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Diffuser une information',
//                     type: 'basic',
//                     icon: 'heroicons_outline:information-circle',
//                     link: '/user/diffusions/diffuser',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Prêts',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-cart',
//                     link: '/user/diffusions/prets',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Poster un nouveau prix',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-cart',
//                     link: '/user/diffusions/poster',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Informations partagées',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/informationspartagees',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Prix partagés',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/prixPartages',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Les Prêts',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/lesprets',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Stats',
//             type: 'collapsable',
//             icon: 'heroicons_outline:chart-pie',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Consommables',
//                     type: 'basic',
//                     icon: 'heroicons_outline:chart-pie',
//                     link: '/user/stats/consommables',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits',
//                     type: 'basic',
//                     icon: 'heroicons_outline:chart-pie',
//                     link: '/user/stats/produits',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Confirmations infos',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/stats/confirmations',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'infos',
//             title: 'Clients',
//             type: 'basic',
//             icon: 'heroicons_outline:menu-alt-2',
//             link: '/user/clients'
//         },
//         {
//             id: 'diffusions',
//             title: 'Gestions',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion des comptes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:user',

//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Ajouter un compte',
//                             type: 'basic',
//                             icon: 'heroicons_outline:user',
//                             link: '/user/gestion/ajouterCompte',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Tous les comptes',
//                             type: 'basic',
//                             icon: 'heroicons_outline:user',
//                             link: '/user/gestion/tousComptes',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Reclamations',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             badge: {
//                 title: (stat.Reclamation.Bytype.Employe ?? 0) + (stat.Reclamation.Bytype.Client ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion Objets',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/gestionObj',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Envoyer une réclamation',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/envoyer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes réclamations',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/mesReclamations',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'diffusions',
//                     title: 'Liste des réclamations',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:cog',
//                     //link: '/apps/help-center',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Réclamations employés',
//                             type: 'basic',
//                             icon: 'heroicons_outline:user',
//                             link: '/user/reclamations/liste/employes',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.Reclamation.Bytype.Employe ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Réclamations clients',
//                             type: 'basic',
//                             icon: 'heroicons_outline:user',
//                             link: '/user/reclamations/liste/clients',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.Reclamation.Bytype.Client ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Paramètres',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Modifier mon profil',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/parametres',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Demandes de produits',
//             type: 'collapsable',
//             icon: 'heroicons_outline:shopping-cart',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeProduit.Virement.VirementSoc ?? 0) + (stat.DemandeProduit.DemandeRecu ?? 0) + (stat.DemandeProduit.ProduitRecu ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 /*  {
//                       id: 'apps.help-center.home',
//                       title: 'Virement',
//                       type: 'basic',
//                       icon: 'heroicons_outline:shopping-bag',
//                       link: '/user/demandes/virement',
//                       exactMatch: true,
//                       badge: {
//                           title: stat.DemandeProduit.Virement.VirementSoc ?? 0,
//                           classes: 'px-2 bg-teal-400 text-black rounded-full'
//                       },
//                   },*/
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander un produit',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demanderProduit',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demandes reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demandesRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.DemandeRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/produitsRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.ProduitRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:shopping-bag',
//                     // link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/envoyees',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes reçus',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/recues',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Consommables',
//             type: 'collapsable',
//             icon: 'heroicons_outline:archive',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeConsommable.Virement ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion consommables',
//                     type: 'basic',
//                     icon: 'heroicons_outline:plus',
//                     link: '/user/consommables/gestion',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Virement consommable',
//                     type: 'basic',
//                     icon: 'heroicons_outline:plus',
//                     link: '/user/consommables/virement',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeConsommable.Virement ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Demander pour une boutique',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/demander/pourBoutique',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/consommables/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/mesDemandes/envoyes',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Réassort',
//             type: 'collapsable',
//             icon: 'heroicons_outline:refresh',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeReassort.Virement ?? 0) + (stat.DemandeReassort.Mesdemandes ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Lancer une demander',
//                     type: 'basic',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     link: '/user/reassort/lancer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:bookmark',
//                     link: '/user/reassort/toutesDemandes',
//                     exactMatch: true
//                 },
//                 /* {
//                      id: 'apps.help-center.home',
//                      title: 'Virement',
//                      type: 'basic',
//                      icon: 'heroicons_outline:tag',
//                      link: '/user/reassort/virement',
//                      badge: {
//                          title: stat.DemandeReassort.Virement ?? 0,
//                          classes: 'px-2 bg-teal-400 text-black rounded-full'
//                      },
//                      exactMatch: true
//                  },*/
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:book-open',
//                     link: '/user/reassort/mesDemandes',
//                     badge: {
//                         title: stat.DemandeReassort.Mesdemandes ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                     exactMatch: true
//                 },
//             ]
//         },

//         {
//             id: 'apps.help-center.home',
//             title: 'Fournisseurs',
//             type: 'basic',
//             icon: 'heroicons_outline:book-open',
//             link: '/user/gestionCommercial/informations/listeFournisseur',
//             exactMatch: true
//         },
//         {
//             id: 'gestionCommercial',
//             title: 'Gestion Commercial',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cash',
//             //link: '/apps/help-center',
//             //link: '/user/gestionCommercial/informations/listeFamille',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Famille',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/informations/listeFamille',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'SousFamille',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeSousFamille',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Catégorie',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeCategories',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Discipline',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeDiscipline',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Couleurs',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeGamme1',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Tailles',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeGamme2',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Genre',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeGenre',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Marque',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeMarque',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Saison',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeSaison',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Article Variable',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:book-open',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Liste',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/Article/listePere',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Ajouter',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/Article/ajouterPere',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Article Simple',
//                     type: 'basic',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     link: '/user/gestionCommercial/Article/ajouterArticle',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Articles',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:book-open',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Liste',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/Article/listeArticles',
//                             exactMatch: true
//                         },
                        
//                     ]
//                 },

                
//             ]
//         },
//         {
//             id: 'apps.help-center.home',
//             title: 'Déconnexion',
//             type: 'basic',
//             icon: 'heroicons_outline:logout',
//             link: '/sign-out',
//             exactMatch: true
//         },
//     ]
//     return adminDashNavigation;
// }

// export function userDashNav(stat: any) {
//     const userNavigation: FuseNavigationItem[] = [
//         {
//             id: 'acceuil',
//             title: 'Acceuil',
//             type: 'basic',
//             icon: 'heroicons_outline:home',
//             link: '/user/dashboards/project',
//             badge: {
//                 title: stat.Information ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//         },
//         {
//             id: 'lien',
//             title: 'Liens',
//             type: 'basic',
//             icon: 'heroicons_outline:link',
//             link: '/user/liens'
//         },
//         {
//             id: 'infos',
//             title: 'Mes infos partagées',
//             type: 'basic',
//             icon: 'heroicons_outline:menu-alt-2',
//             link: '/user/infospartages'
//         },
//         {
//             id: 'infos',
//             title: 'Prix partagées',
//             type: 'basic',
//             icon: 'heroicons_outline:shopping-cart',
//             link: '/user/diffusions/prixPartages'
//         },
//         {
//             id: 'diffusions',
//             title: 'Reclamations',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Envoyer une réclamation',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/envoyer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes réclamations',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/mesReclamations',
//                     exactMatch: true
//                 }
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Paramètres',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Modifier mon profil',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/parametres',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Demandes de produits',
//             type: 'collapsable',
//             icon: 'heroicons_outline:shopping-cart',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeProduit.DemandeRecu ?? 0) + (stat.DemandeProduit.ProduitRecu ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander un produit',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demanderProduit',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demandes reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demandesRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.DemandeRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/produitsRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.ProduitRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:shopping-bag',
//                     // link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/envoyees',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes reçus',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/recues',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Consommables',
//             type: 'collapsable',
//             icon: 'heroicons_outline:archive',
//             //link: '/apps/help-center',
//             badge: {
//                 title: stat.DemandeConsommable.Recu ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Simple demande',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/demander/simple',
//                             exactMatch: true
//                         }
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Consommable reçu',
//                     type: 'basic',
//                     icon: 'heroicons_outline:reply',
//                     link: '/user/consommables/recu',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeConsommable.Recu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/mesDemandes/envoyes',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Réassort',
//             type: 'collapsable',
//             icon: 'heroicons_outline:refresh',
//             //link: '/apps/help-center',
//             badge: {
//                 title: stat.DemandeReassort.Mesdemandes ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:book-open',
//                     link: '/user/reassort/mesDemandes',
//                     badge: {
//                         title: stat.DemandeReassort.Mesdemandes ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'apps.help-center.home',
//             title: 'Déconnexion',
//             type: 'basic',
//             icon: 'heroicons_outline:logout',
//             link: '/sign-out',
//             exactMatch: true
//         },
//     ];
//     return userNavigation;
// }

// export function directeurNav(stat: any) {
//     const directeurNavigation: FuseNavigationItem[] = [
//         {
//             id: 'acceuil',
//             title: 'Acceuil',
//             type: 'basic',
//             icon: 'heroicons_outline:home',
//             link: '/user/dashboards/project',
//             badge: {
//                 title: stat.Information ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//         },
//         /*{
//             id: 'diffusions',
//             title: 'Notifications',
//             type: 'collapsable',
//             icon: 'heroicons_outline:chart-pie',
//             badge: {
//                 title: (stat.DemandeProduit.Bystatut.Encours ?? 0) + (stat.DemandeProduit.Bystatut.Virement ?? 0) +  (stat.DemandeConsommable.Entransit ?? 0) 
//                 + (stat.DemandeReassort.Bystatut.Encours ?? 0) + (stat.DemandeReassort.Bystatut.Virement ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             // link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Produits',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:chart-pie',
//                     //link: '/apps/help-center',
//                     //exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Encours',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/produits/encours',
//                             badge: {
//                                 title: stat.DemandeProduit.Bystatut.Encours ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                             //exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/produits/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeProduit.Bystatut.Virement ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Consommable',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:chart-pie',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/consommable/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeConsommable.Entransit ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Réassort',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:user',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Encours',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/reassort/encours',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeReassort.Bystatut.Encours ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/reassort/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeReassort.Bystatut.Virement ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//             ],
//         },*/

//         {
//             id: 'lien',
//             title: 'Liens',
//             type: 'basic',
//             icon: 'heroicons_outline:link',
//             link: '/user/liens'
//         },

//         {
//             id: 'infos',
//             title: 'Mes infos partagées',
//             type: 'basic',
//             icon: 'heroicons_outline:menu-alt-2',
//             link: '/user/infospartages'
//         },
//         {
//             id: 'diffusions',
//             title: 'Reclamations',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Envoyer une réclamation',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/envoyer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes réclamations',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/mesReclamations',
//                     exactMatch: true
//                 }
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Les diffusions',
//             type: 'collapsable',
//             icon: 'heroicons_outline:share',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Informations partagées',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/informationspartagees',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Prix partagés',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/prixPartages',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Les Prêts',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/lesprets',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Stats',
//             type: 'collapsable',
//             icon: 'heroicons_outline:chart-pie',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Consommables',
//                     type: 'basic',
//                     icon: 'heroicons_outline:chart-pie',
//                     link: '/user/stats/consommables',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits',
//                     type: 'basic',
//                     icon: 'heroicons_outline:chart-pie',
//                     link: '/user/stats/produits',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Confirmations infos',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/stats/confirmations',
//                     exactMatch: true
//                 },
//             ]
//         },


//         {
//             id: 'diffusions',
//             title: 'Paramètres',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Modifier mon profil',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/parametres',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Demandes de produits',
//             type: 'collapsable',
//             icon: 'heroicons_outline:shopping-cart',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeProduit.DemandeRecu ?? 0) + (stat.DemandeProduit.ProduitRecu ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander un produit',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demanderProduit',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demandes reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demandesRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.DemandeRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/produitsRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.ProduitRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:shopping-bag',
//                     // link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/envoyees',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes reçus',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/recues',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Consommables',
//             type: 'collapsable',
//             icon: 'heroicons_outline:archive',
//             //link: '/apps/help-center',
//             badge: {
//                 title: stat.DemandeConsommable.Recu ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Simple demande',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/demander/simple',
//                             exactMatch: true
//                         }
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Consommable reçu',
//                     type: 'basic',
//                     icon: 'heroicons_outline:reply',
//                     link: '/user/consommables/recu',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeConsommable.Recu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/mesDemandes/envoyes',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Réassort',
//             type: 'collapsable',
//             icon: 'heroicons_outline:refresh',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeReassort.Mesdemandes ?? 0) + (stat.DemandeReassort.Virement ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Lancer une demander',
//                     type: 'basic',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     link: '/user/reassort/lancer',
//                     exactMatch: true
//                 },
//                 // {
//                 //     id: 'apps.help-center.home',
//                 //     title: 'Virement',
//                 //     type: 'basic',
//                 //     icon: 'heroicons_outline:tag',
//                 //     link: '/user/reassort/virement',
//                 //     badge: {
//                 //         title: stat.DemandeReassort.Virement ?? 0,
//                 //         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                 //     },
//                 //     exactMatch: true
//                 // },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:book-open',
//                     link: '/user/reassort/mesDemandes',
//                     badge: {
//                         title: stat.DemandeReassort.Mesdemandes ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'apps.help-center.home',
//             title: 'Déconnexion',
//             type: 'basic',
//             icon: 'heroicons_outline:logout',
//             link: '/sign-out',
//             exactMatch: true
//         },
//     ];
//     return directeurNavigation;
// }

// export function managerNav(stat: any) {
//     const managerNavigation: FuseNavigationItem[] = [
//         {
//             id: 'acceuil',
//             title: 'Acceuil',
//             type: 'basic',
//             icon: 'heroicons_outline:home',
//             link: '/user/dashboards/project'
//         },
//         {
//             id: 'diffusions',
//             title: 'Notifications',
//             type: 'collapsable',
//             icon: 'heroicons_outline:chart-pie',
//             badge: {
//                 title: (stat.DemandeProduit.Bystatut.Encours ?? 0) + (stat.DemandeProduit.Bystatut.Virement ?? 0) + (stat.DemandeConsommable.Entransit ?? 0)
//                     + (stat.DemandeReassort.Bystatut.Encours ?? 0) + (stat.DemandeReassort.Bystatut.Virement ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             // link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Produits',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:chart-pie',
//                     //link: '/apps/help-center',
//                     //exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Encours',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/produits/encours',
//                             badge: {
//                                 title: stat.DemandeProduit.Bystatut.Encours ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                             //exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/produits/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeProduit.Bystatut.Virement ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Consommable',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:chart-pie',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/consommable/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeConsommable.Entransit ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Dem. Réassort',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:user',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Encours',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/reassort/encours',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeReassort.Bystatut.Encours ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'En transit',
//                             type: 'basic',
//                             icon: 'heroicons_outline:chart-pie',
//                             link: '/user/notifications/reassort/entransit',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.DemandeReassort.Bystatut.Virement ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//             ],
//         },

//         {
//             id: 'lien',
//             title: 'Liens',
//             type: 'basic',
//             icon: 'heroicons_outline:link',
//             link: '/user/liens'
//         },

//         /* {
//              id: 'infos',
//              title: 'Mes infos partagées',
//              type: 'basic',
//              icon: 'heroicons_outline:menu-alt-2',
//              link: '/user/infospartages'
//          },*/
//         {
//             id: 'diffusions',
//             title: 'Les diffusions',
//             type: 'collapsable',
//             icon: 'heroicons_outline:share',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion consommables',
//                     type: 'basic',
//                     icon: 'heroicons_outline:plus',
//                     link: '/user/consommables/gestion',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Diffuser une information',
//                     type: 'basic',
//                     icon: 'heroicons_outline:information-circle',
//                     link: '/user/diffusions/diffuser',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Prêts',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-cart',
//                     link: '/user/diffusions/prets',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Poster un nouveau prix',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-cart',
//                     link: '/user/diffusions/poster',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Informations partagées',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/informationspartagees',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Prix partagés',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/prixPartages',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Les Prêts',
//                     type: 'basic',
//                     icon: 'heroicons_outline:menu-alt-2',
//                     link: '/user/diffusions/lesprets',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Stats',
//             type: 'collapsable',
//             icon: 'heroicons_outline:chart-pie',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Consommables',
//                     type: 'basic',
//                     icon: 'heroicons_outline:chart-pie',
//                     link: '/user/stats/consommables',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits',
//                     type: 'basic',
//                     icon: 'heroicons_outline:chart-pie',
//                     link: '/user/stats/produits',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Confirmations infos',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/stats/confirmations',
//                     exactMatch: true
//                 },
//             ]
//         },

//         {
//             id: 'diffusions',
//             title: 'Reclamations',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.Reclamation.Bytype.Employe ?? 0) + (stat.Reclamation.Bytype.Client ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion Objets',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/gestionObj',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Envoyer une réclamation',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/envoyer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes réclamations',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/mesReclamations',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'diffusions',
//                     title: 'Liste des réclamations',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:cog',
//                     //link: '/apps/help-center',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Réclamations employés',
//                             type: 'basic',
//                             icon: 'heroicons_outline:user',
//                             link: '/user/reclamations/liste/employes',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.Reclamation.Bytype.Employe ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Réclamations clients',
//                             type: 'basic',
//                             icon: 'heroicons_outline:user',
//                             link: '/user/reclamations/liste/clients',
//                             exactMatch: true,
//                             badge: {
//                                 title: stat.Reclamation.Bytype.Client ?? 0,
//                                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//                             },
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Paramètres',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Modifier mon profil',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/parametres',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Demandes de produits',
//             type: 'collapsable',
//             icon: 'heroicons_outline:shopping-cart',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeProduit.Virement.VirementSoc ?? 0) + (stat.DemandeProduit.DemandeRecu ?? 0) + (stat.DemandeProduit.ProduitRecu ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 /*  {
//                       id: 'apps.help-center.home',
//                       title: 'Virement',
//                       type: 'basic',
//                       icon: 'heroicons_outline:shopping-bag',
//                       link: '/user/demandes/virement',
//                       exactMatch: true,
//                       badge: {
//                           title: stat.DemandeProduit.Virement.VirementSoc ?? 0,
//                           classes: 'px-2 bg-teal-400 text-black rounded-full'
//                       },
//                   },*/
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander un produit',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demanderProduit',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demandes reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demandesRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.DemandeRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/produitsRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.ProduitRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:shopping-bag',
//                     // link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/envoyees',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes reçus',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/recues',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Consommables',
//             type: 'collapsable',
//             icon: 'heroicons_outline:archive',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeConsommable.Virement ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Virement consommable',
//                     type: 'basic',
//                     icon: 'heroicons_outline:plus',
//                     link: '/user/consommables/virement',
//                     exactMatch: true,
//                     badge: {
//                         title: (stat.DemandeConsommable.Virement ?? 0),
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Simple demande',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/demander/simple',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Demander pour une boutique',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/demander/pourBoutique',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/consommables/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/mesDemandes/envoyes',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Réassort',
//             type: 'collapsable',
//             icon: 'heroicons_outline:refresh',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeReassort.Virement ?? 0) + (stat.DemandeReassort.Mesdemandes ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Lancer une demander',
//                     type: 'basic',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     link: '/user/reassort/lancer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:bookmark',
//                     link: '/user/reassort/toutesDemandes',
//                     exactMatch: true
//                 },
//                 /*  {
//                       id: 'apps.help-center.home',
//                       title: 'Virement',
//                       type: 'basic',
//                       icon: 'heroicons_outline:tag',
//                       link: '/user/reassort/virement',
//                       badge: {
//                           title: stat.DemandeReassort.Virement ?? 0,
//                           classes: 'px-2 bg-teal-400 text-black rounded-full'
//                       },
//                       exactMatch: true
//                   },*/
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:book-open',
//                     link: '/user/reassort/mesDemandes',
//                     badge: {
//                         title: stat.DemandeReassort.Mesdemandes ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'gestionCommercial',
//             title: 'Gestion Commercial',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cash',
//             //link: '/apps/help-center',
//             //link: '/user/gestionCommercial/informations/listeFamille',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Gestion',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Famille',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/informations/listeFamille',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Fournisseurs',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeFournisseur',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Catégorie',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeCategories',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Discipline',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeDiscipline',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Couleur',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeGamme1',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Taille',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeGamme2',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Genre',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeGenre',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Marque',
//                             type: 'basic',
//                             icon: 'heroicons_outline:book-open',
//                             link: '/user/gestionCommercial/informations/listeMarque',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Article Variable',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:book-open',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Liste',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/Article/listePere',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Ajouter',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/Article/ajouterPere',
//                             exactMatch: true
//                         },
//                     ]
//                 },
                
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Article Simple',
//                     type: 'basic',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     link: '/user/gestionCommercial/Article/ajouterArticle',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Articles',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:book-open',
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Liste',
//                             type: 'basic',
//                             icon: 'heroicons_outline:lightning-bolt',
//                             link: '/user/gestionCommercial/Article/listeArticles',
//                             exactMatch: true
//                         },
//                     ]
//                 },
                
//             ]
//         },
//         {
//             id: 'apps.help-center.home',
//             title: 'Déconnexion',
//             type: 'basic',
//             icon: 'heroicons_outline:logout',
//             link: '/sign-out',
//             exactMatch: true
//         },
//     ];
//     return managerNavigation;
// }

// export function clientNav(loggedIn: boolean) {
//     const clientNavigation: FuseNavigationItem[] = [
//         // {
//         //     id: 'acceuil',
//         //     title: 'Acceuil',
//         //     type: 'basic',
//         //     tooltip: 'Acceuil',
//         //     icon: 'heroicons_outline:home',
//         //     link: '/client/acceuil',
//         // },
//         // {
//         //     id: 'acceuil',
//         //     title: 'Reclamation',
//         //     tooltip: 'Reclamation',
//         //     type: 'basic',
//         //     icon: 'heroicons_outline:pencil',
//         //     link: '/client/liste/55'
//         // },
//         loggedIn ? {
//             id: 'acceuil',
//             title: 'Deconnexion',
//             tooltip: 'Deconnexion',
//             type: 'basic',
//             icon: 'heroicons_outline:logout',
//             link: '/client/logout'
//         } : {
//             id: 'acceuil',
//             title: 'Connecter',
//             tooltip: 'Connecter',
//             type: 'basic',
//             icon: 'heroicons_outline:login',
//             link: '/client/contact'
//         },
//     ]
//     return clientNavigation;
// }

// export function superviseurDashNav(stat: any) {
//     const superviseurNavigation: FuseNavigationItem[] = [
//         {
//             id: 'acceuil',
//             title: 'Acceuil',
//             type: 'basic',
//             icon: 'heroicons_outline:home',
//             link: '/user/dashboards/project',
//             badge: {
//                 title: stat.Information ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//         },
//         {
//             id: 'lien',
//             title: 'Liens',
//             type: 'basic',
//             icon: 'heroicons_outline:link',
//             link: '/user/liens'
//         },
//         {
//             id: 'infos',
//             title: 'Mes infos partagées',
//             type: 'basic',
//             icon: 'heroicons_outline:menu-alt-2',
//             link: '/user/infospartages'
//         },
//         {
//             id: 'infos',
//             title: 'Prix partagées',
//             type: 'basic',
//             icon: 'heroicons_outline:shopping-cart',
//             link: '/user/diffusions/prixPartages'
//         },
//         {
//             id: 'diffusions',
//             title: 'Reclamations',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Envoyer une réclamation',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/envoyer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes réclamations',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/reclamations/mesReclamations',
//                     exactMatch: true
//                 }
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Paramètres',
//             type: 'collapsable',
//             icon: 'heroicons_outline:cog',
//             //link: '/apps/help-center',
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Modifier mon profil',
//                     type: 'basic',
//                     icon: 'heroicons_outline:user',
//                     link: '/user/parametres',
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Demandes de produits',
//             type: 'collapsable',
//             icon: 'heroicons_outline:shopping-cart',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeProduit.DemandeRecu ?? 0) + (stat.DemandeProduit.ProduitRecu ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander un produit',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demanderProduit',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demandes reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/demandesRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.DemandeRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Produits reçus',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/produitsRecues',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeProduit.ProduitRecu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/demandes/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:shopping-bag',
//                     // link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/envoyees',
//                             exactMatch: true
//                         },
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes reçus',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/demandes/mesDemandes/recues',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Consommables',
//             type: 'collapsable',
//             icon: 'heroicons_outline:archive',
//             //link: '/apps/help-center',
//             badge: {
//                 title: stat.DemandeConsommable.Recu ?? 0,
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Demander',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Simple demande',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/demander/simple',
//                             exactMatch: true
//                         }
//                     ]
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:shopping-bag',
//                     link: '/user/consommables/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Consommable reçu',
//                     type: 'basic',
//                     icon: 'heroicons_outline:reply',
//                     link: '/user/consommables/recu',
//                     exactMatch: true,
//                     badge: {
//                         title: stat.DemandeConsommable.Recu ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'collapsable',
//                     icon: 'heroicons_outline:plus',
//                     //link: '/apps/help-center',
//                     exactMatch: true,
//                     children: [
//                         {
//                             id: 'apps.help-center.home',
//                             title: 'Mes demandes envoyées',
//                             type: 'basic',
//                             icon: 'heroicons_outline:shopping-bag',
//                             link: '/user/consommables/mesDemandes/envoyes',
//                             exactMatch: true
//                         },
//                     ]
//                 },
//             ]
//         },
//         {
//             id: 'diffusions',
//             title: 'Réassort',
//             type: 'collapsable',
//             icon: 'heroicons_outline:refresh',
//             //link: '/apps/help-center',
//             badge: {
//                 title: (stat.DemandeReassort.Virement ?? 0) + (stat.DemandeReassort.Mesdemandes ?? 0),
//                 classes: 'px-2 bg-teal-400 text-black rounded-full'
//             },
//             children: [
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Lancer une demander',
//                     type: 'basic',
//                     icon: 'heroicons_outline:lightning-bolt',
//                     link: '/user/reassort/lancer',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Toutes les demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:bookmark',
//                     link: '/user/reassort/toutesDemandes',
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'Virement',
//                     type: 'basic',
//                     icon: 'heroicons_outline:tag',
//                     link: '/user/reassort/virement',
//                     badge: {
//                         title: stat.DemandeReassort.Virement ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                     exactMatch: true
//                 },
//                 {
//                     id: 'apps.help-center.home',
//                     title: 'mes demandes',
//                     type: 'basic',
//                     icon: 'heroicons_outline:book-open',
//                     link: '/user/reassort/mesDemandes',
//                     badge: {
//                         title: stat.DemandeReassort.Mesdemandes ?? 0,
//                         classes: 'px-2 bg-teal-400 text-black rounded-full'
//                     },
//                     exactMatch: true
//                 },
//             ]
//         },
//         {
//             id: 'apps.help-center.home',
//             title: 'Déconnexion',
//             type: 'basic',
//             icon: 'heroicons_outline:logout',
//             link: '/sign-out',
//             exactMatch: true
//         },
//     ];
//     return superviseurNavigation;
// }