import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseMediaWatcherService } from '@fuse/services/media-watcher';
import { FuseNavigationItem, FuseNavigationService, FuseVerticalNavigationComponent } from '@fuse/components/navigation';
import { NavigationService } from 'app/core/navigation/navigation.service';
import { adherentNavigation, personnelSportifNavigation, superNavigation } from '../../navigationData';
import { AuthService } from 'app/core/auth/auth.service';
import { NavigationCompService } from 'app/core/services/navigation-comp.service';

@Component({
    selector: 'classic-layout',
    templateUrl: './classic.component.html',
    encapsulation: ViewEncapsulation.None,
    template: `<app-child (reload)="handleSomeEvent($event)"></app-child>`
})
export class ClassicLayoutComponent implements OnInit, OnDestroy {
    isScreenSmall: boolean;
    //navigation: Navigation;
    private _unsubscribeAll: Subject<any> = new Subject<any>();


    //userNavigation: FuseNavigationItem[] = userNavigation;
    superNavigation: FuseNavigationItem[] = superNavigation;
   //adminBoutiqueNavigation: FuseNavigationItem[] = adminBoutiqueNavigation
    //directeurNavigation: FuseNavigationItem[] = directeurNavigation;
    //managerNavigation: FuseNavigationItem[] = managerNavigation;
    //managerdashNavigation: FuseNavigationItem[] = managerdashNavigation;
    //superviseurDashboard: FuseNavigationItem[] = superviseurdashboard;
    selectedNavigation: FuseNavigationItem[];

    currentBoutique: string;

    stats: any;
    selectedFunction:Function;

    public setNavigation(role: string) {

    }
    
    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private _router: Router,
        private _navigationService: NavigationService,
        private _fuseMediaWatcherService: FuseMediaWatcherService,
        private _fuseNavigationService: FuseNavigationService,
        private _navigationCompService: NavigationCompService,
    ) {
        this._navigationCompService.refresh.pipe(takeUntil(this._unsubscribeAll)).subscribe(value => this.prepareStats());
    }

    salleDeSport: string    ;

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for current year
     */
    get currentYear(): number {
        return new Date().getFullYear();
    }
    handleSomeEvent(event: any): void {
        // Use your service to make a request to your backend
        // The event here carries the emitted data by someEvent
      }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.salleDeSport = this.authService.userData.salleDeSport.nom
     
        //this.prepareNavigation()
        this.prepareStats()
    }

    prepareNavigation() {
        //this.prepareStats()

        // if (/^\/user/.test(this._router.url) && this.authService.userBoutique === null) {
        //     this._router.navigate(['/'])
        // }
        // Subscribe to navigation data
        // this._navigationService.navigation$
        //     .pipe(takeUntil(this._unsubscribeAll))
        //     .subscribe((navigation: Navigation) => {
        //         this.navigation = navigation;
        //     });

        // Subscribe to media changes
        this._fuseMediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({ matchingAliases }) => {

                // Check if the screen is small
                this.isScreenSmall = !matchingAliases.includes('md');
            });

            console.log(this.authService.userData.roles.includes("ROLE_ADHERENT") )

        if (this.authService.userData.roles.includes("ROLE_GERANT")|| this.authService.userData.roles.includes("ROLE_PERSONNEL_ADMINISTRATIF")) this.selectedNavigation = superNavigation;
        else if (this.authService.userData.roles.includes("ROLE_ADHERENT")) this.selectedNavigation = adherentNavigation;
        else if (this.authService.userData.roles.includes("ROLE_PERSONNEL_SPORTIF")) this.selectedNavigation = personnelSportifNavigation;
     //   else if (/^\/superviseur/.test(this._router.url)) this.selectedNavigation = superviseurdashboard;
        else {
            const roles: string[] = this.authService.userData.roles;
            // if (roles.includes('ROLE_ADMIN')) {
            //     this.selectedNavigation =  adminDashNav(this.stats);
            // }else if (roles.includes('ROLE_USER')) {
            //     this.selectedNavigation = userDashNav(this.stats);
            // }

        }
      
    }

    prepareStats() {
        // if (/^\/user/.test(this._router.url)) {
        //     this._navigationCompService.getStatistiques(this.authService.userBoutique.id.toString())
        //         .subscribe(
        //             success => {
        //                 this.stats = success
        //                 console.log(this.stats)
        //                 this.prepareNavigation()
        //             }
        //         )
        // }else{
            this.prepareNavigation()
     //   }
    }

    

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle navigation
     *
     * @param name
     */
    toggleNavigation(name: string): void {
        // Get the navigation
        const navigation = this._fuseNavigationService.getComponent<FuseVerticalNavigationComponent>(name);

        if (navigation) {
            // Toggle the opened status
            navigation.toggle();
        }
    }
}
