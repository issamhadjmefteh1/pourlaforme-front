import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

const URL_ABONNEMENT = "/api/abonnement/";



@Injectable({
  providedIn: 'root'
})
export class AbonnementService {

  constructor(private _httpClient: HttpClient,
    ) { }

    getAbonnementByAderent(id): Observable<any> {
      return this._httpClient.get (environment.apiUrl + URL_ABONNEMENT +"getAbonnementAdherent/"+ id);
    }

    saveAbonnement(abonnement): Observable<any> {
      return this._httpClient.post (environment.apiUrl + URL_ABONNEMENT + "createAbonnement", abonnement)
    }
}
