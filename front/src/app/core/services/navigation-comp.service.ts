import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationCompService {
  
  refresh: BehaviorSubject<boolean> = new BehaviorSubject(false);

  readonly url:string = environment.apiUrl +"/api/notification";
 
  constructor(private httpClient:HttpClient) { }

  getStatistiques(id:string):Observable<any>{
    return this.httpClient.get(this.url+"/GetAll/"+id);
  }

  ReloadChange(){
    this.refresh.next (true);
  }

}
