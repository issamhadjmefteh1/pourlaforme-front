import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


const URL_SEANCE = "/api/seance/";

@Injectable({
  providedIn: 'root'
})

export class SeanceService {

    constructor(private _httpClient: HttpClient,
      
    ) { }
  
    getListSeancesByIdSalleCours(id): Observable<any>{
      return this._httpClient.get(environment.apiUrl + URL_SEANCE + "list/" + id);
    }
}
