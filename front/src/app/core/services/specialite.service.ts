import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

const URL_SPECIALITE = "/api/specialite/";


@Injectable({
  providedIn: 'root'
})
export class SpecialiteService {

  constructor(private _httpClient: HttpClient,
    
  ) { }

  saveSpecialite(specialite): Observable<any>{
    return this._httpClient.post(environment.apiUrl + URL_SPECIALITE + "specialites", specialite);
  }

  getSpecialites(): Observable<any> {
      return this._httpClient.get(environment.apiUrl + URL_SPECIALITE + 'specialites')
  }

  deleteSpecialite(id): Observable<any>{
      return this._httpClient.delete(environment.apiUrl + URL_SPECIALITE + 'specialites/' + id )
  }

  getSpecilites(id): Observable<any> {
    return this._httpClient.get(environment.apiUrl + URL_SPECIALITE + "specialites/" + id);
  }

}