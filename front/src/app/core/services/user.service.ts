import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

const URL_USER = "/api/user/";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _httpClient: HttpClient,
    
  ) { }

  getUsers() : Observable<any> {
    return this._httpClient.get(environment.apiUrl + URL_USER + "users");
  }

  activate(idUser: number) :Observable<any> {
    return this._httpClient.get(environment.apiUrl + URL_USER + "activate/" + idUser);
  }
  getUserById(id): Observable<any>{
    return this._httpClient.get(environment.apiUrl + URL_USER + "getOne/" + id);
  }
  updateUser(user): Observable<any> {
    return this._httpClient.post(environment.apiUrl + URL_USER + "updateUser", user);
  }
}
