import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const URL_SALLE_COURS= "/api/salleDeCours/";

@Injectable({
  providedIn: 'root'
})
export class SalleDeCoursService {

  constructor(private _httpClient: HttpClient,
    
  ) { }

  getListSalleDeCoursBySalleDeSport(id): Observable<any>{
    return this._httpClient.get(environment.apiUrl + URL_SALLE_COURS + "list/" + id);
  }
}
