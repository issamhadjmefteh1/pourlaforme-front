import { Seance } from './../models/Seance';
import { Planning } from './../models/Planning';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';

const URL_PLANNING= "/api/planning/";

@Injectable({
  providedIn: 'root'
})

export class PlanningService {

  constructor(private _httpClient: HttpClient
    
  ) { }

  getListPlanningByIdSalle(id): Observable<any>{
    return this._httpClient.get(environment.apiUrl + URL_PLANNING + "list/" + id);
  }

  addPlanning(planning): Observable<any> {
    let plan = new Planning();
    let seance = new Seance();
    seance.idSeance = planning.seance
    console.log(seance)
    plan.seance = seance
 //   plan.datePlannif = this.datePipe.transform(planning.datePlannif,'dd/MM/yyyy')
    plan.heureDebut = planning.heureDebut
    plan.heureFin = planning.heureFin
    return this._httpClient.post (environment.apiUrl + URL_PLANNING + "add", plan)
  }
}
