import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

const URL_ROLE = "/api/role/";



@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private _httpClient: HttpClient,
    ) { }

    getRoles(): Observable<any> {
      return this._httpClient.get (environment.apiUrl + URL_ROLE +"roles");
    }
}
