import { TestBed } from '@angular/core/testing';

import { SalleDeCoursService } from './salle-de-cours.service';

describe('SalleDeCoursService', () => {
  let service: SalleDeCoursService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalleDeCoursService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
