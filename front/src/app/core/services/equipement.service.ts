import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

const URL_EQUIPEMENT = "/api/equipement/";


@Injectable({
  providedIn: 'root'
})
export class EquipementService {

  constructor(private _httpClient: HttpClient,
    
  ) { }

  saveEquipement(equipement):Observable<any> {
      return this._httpClient.post(environment.apiUrl + URL_EQUIPEMENT + "equipements", equipement);
  }
  getEquipements():Observable<any> {
      return this._httpClient.get(environment.apiUrl + URL_EQUIPEMENT + "equipements")
  }

  deleteEquipement(id): Observable<any>{
      return this._httpClient.delete(environment.apiUrl + URL_EQUIPEMENT + "equipements/" + id );
  }

  getEquipement(id) : Observable<any> {
    return this._httpClient.get(environment.apiUrl + URL_EQUIPEMENT + "equipements/" + id)
  }

}
