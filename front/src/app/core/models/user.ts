import { Role } from "./Role";

export interface User {

    username: string;
    email: string
    lastname: string;
    roles: Array<Role>;
    password: string;

}